import React from 'react';
import { View, StyleSheet, StatusBar } from 'react-native';

import { BaseButton } from 'react-native-gesture-handler';
import i18 from '../i18';
import { BaseText, Logo } from '../component/custom/BaseText';
import { BaseInput } from '../component/custom/BaseInput';
import { asset, LargeDimension, screenHeight, } from '../config';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import LinearGradient from 'react-native-linear-gradient';
import { Head } from '../component/custom/Nav';

class Login extends React.Component {

    signUp = () => {
        this.props.navigation.navigate('verifyOTP');
    }

    render() {
        return <View style={styles.container}>
            <StatusBar backgroundColor={'#4b93f4'} />
            <LinearGradient colors={['#4b93f4', '#2b7bff']} style={styles.gadient}>
                <Head
                    back
                    {...this.props}
                />
                <BaseText children={i18.t('signUp')} style={styles.titlePage} size='lg' />
                <KeyboardAwareScrollView
                    extraScrollHeight={50}
                    scrollEnabled={false}
                    enableOnAndroid={true}
                >
                    <Logo style={styles.wrapLogo} />
                    <View style={styles.wrapInput}>
                        <BaseInput placeholder={i18.t('user_name')} icon={asset.userIcon} references={r => this.inputUserName = r}
                            returnKeyType={"next"}
                            onSubmitEditing={() => {
                                this.inputPassword.focus()
                            }}
                        />
                        <BaseInput placeholder={i18.t('password')} icon={asset.tel} references={r => this.inputPassword = r} containerStyle={{
                            marginTop: 24
                        }}
                            maxLength={15}
                            returnKeyType={"next"}
                            onSubmitEditing={() => {
                                this.confirmPassword.focus()
                            }}
                            pass
                        />
                        <BaseInput placeholder={i18.t('confirmPassword')} icon={asset.passwordIcon} references={r => this.confirmPassword = r} containerStyle={{
                            marginTop: 24,
                        }}
                            maxLength={15}
                            returnKeyType={"done"}
                            pass
                        />
                    </View>
                </KeyboardAwareScrollView>
            </LinearGradient >
            <View style={styles.wrapButtonSecton}>
                <View style={styles.wrapButton}>
                    <BaseButton
                        onPress={() => { }}
                        style={styles.button}>
                        <BaseText style={styles.btnText} size='md'>Facebook</BaseText>
                    </BaseButton>
                </View>
                <View style={{
                    ...styles.wrapButton, backgroundColor: 'rgb(255,143,60)',
                    shadowColor: 'rgb(255,143,60)',
                }}>
                    <BaseButton onPress={this.signIn}
                        onPress={this.signUp}
                        style={{
                            ...styles.button, backgroundColor: 'rgb(255,143,60)',
                        }}>
                        <BaseText style={styles.btnText} size='md'>{i18.t('signUp')}</BaseText>
                    </BaseButton>
                </View>
            </View>
        </View >
    }
}

let styles = StyleSheet.create({
    container: { flex: 1, minHeight: 0.9 * screenHeight, backgroundColor: 'rgb(252,252,252)' },
    gadient: { flex: 1, borderBottomRightRadius: 20, borderBottomLeftRadius: 20 },
    titlePage: { color: 'white', fontWeight: '500', marginTop: LargeDimension ? 8 : 0, marginLeft: 20 },
    wrapInput: { paddingHorizontal: 20, justifyContent: 'center', flex: 1, marginTop: 20 },
    wrapButtonSecton: {
        flexDirection: 'row', paddingHorizontal: 26, justifyContent: 'space-between', marginTop: 24, marginBottom: 52,
    },
    wrapButton: {
        alignSelf: 'center', width: LargeDimension ? 150 : 125, height: LargeDimension ? 46 : 38,
        justifyContent: 'center', alignItems: 'center', backgroundColor: '#438ff6',
        borderRadius: 23,
        shadowColor: '#438ff6',
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: .5,
        shadowRadius: 4.65,

        elevation: 4,
    },
    button: {
        justifyContent: 'center', alignItems: 'center', backgroundColor: '#438ff6',
        borderRadius: 23, width: '100%', height: '100%'
    },
    btnText: { color: 'white', fontWeight: '500' },
    wrapLogo: { alignSelf: 'center', marginTop: 12 }
})

export default Login