import React from 'react'
import { View, Image } from 'react-native'
import { Head } from '../component/custom/Nav';
import { BaseText } from '../component/custom/BaseText';
import { asset } from '../config';
import LinearGradient from 'react-native-linear-gradient'

export default class ViewTest extends React.Component {
    render() {
        return <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 1 }} colors={['#7fb5ff', '#144891',]} style={{ flex: 1 }}>
            <Head
                left={<Image source={asset.menu} style={{ height: 14, width: 16, tintColor: 'white' }} />}
            // onLeftPress={() => { this.props.navigation.push('booking') }}

            />
            <View style={{ flex: 1, paddingHorizontal: 20, }}>
                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 24, }}>
                    <Image source={asset.defautAVT} style={{ height: 40, width: 40, borderRadius: 20, }} />
                    <View style={{ flex: 1, marginLeft: 16 }}>
                        <BaseText size='lg' style={{ color: 'white', fontWeight: '900' }}>Norman Wong</BaseText>
                        <BaseText style={{ color: 'white', marginTop: 4 }}>Quên mật khẩu</BaseText>
                    </View>
                </View>

                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 48, }}>
                    <Image source={asset.defautAVT} style={{ height: 32, width: 32, borderRadius: 16, }} />
                    <View style={{ flex: 1, marginLeft: 16 }}>
                        <BaseText size='md' style={{ color: 'white', fontWeight: '900', marginTop: 4 }}>17 Feb 2019</BaseText>
                        <BaseText style={{ color: 'white' }}>Khám tai mũi họng</BaseText>
                    </View>
                </View>

                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 48, }}>
                    <View style={{ flex: 1 }}>
                        <BaseText size='md' style={{ color: 'white', fontWeight: '900', marginTop: 4 }}>17 Feb 2019</BaseText>
                        <BaseText style={{ color: 'white' }}>Khám tai mũi họng</BaseText>
                    </View>
                    <Image source={asset.tel} style={{ height: 15, width: 13 }} />
                    <Image source={asset.tel} style={{ height: 31, width: 31 }} />
                </View>
            </View>
        </LinearGradient>
    }
}