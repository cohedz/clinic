import React from 'react';
import { View, Image, FlatList, ScrollView } from "react-native";
import config, { asset, fontSize, LargeDimension, screenWidth, fontFamily } from '../../config';
import { BaseText } from '../../component/custom/BaseText';
import i18 from '../../i18'
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';
import { BaseTextInput } from '../../component/custom/BaseInput';
import { LocaleConfig } from 'react-native-calendars';


export default class SelectTime extends React.PureComponent {

    componentWillMount() {

        LocaleConfig.locales['VN'] = {
            monthNames: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12',],
            monthNamesShort: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12',],
            dayNames: ['Thứ hai', 'Thứ ba', 'Thứ tư', 'Thứ năm', 'Thứ sáu', 'Thứ bảy', 'Chủ nhật'],
            dayNamesShort: [' CN', ' T2', ' T3', ' T4', ' T5', ' T6', ' T7',]
        };

        LocaleConfig.defaultLocale = 'VN';
    }

    render() {
        return <ScrollView>
            <View style={{ paddingHorizontal: 20, flex: 1 }}>
                <BaseText children={i18.t('reasonBooking')} style={{ marginTop: 46, color: '#fff' }} />
                <View style={{
                    minHeight: 110, width: '100%', backgroundColor: 'rgba(255,255,255,0.3)',
                    marginTop: 8, paddingHorizontal: 18, borderRadius: 12, paddingVertical: 14.2, flexDirection: 'row'
                }}>
                    <Image source={asset.pencil} style={{ height: 24, width: 24, marginRight: 10 }} />
                    <BaseTextInput numberOfLines={3} multiline placeholder='Viết gì đó' />
                </View>
                <Calendar
                    style={{
                        borderRadius: 12,
                        marginVertical: 20,
                        backgroundColor: 'rgba(255,255,255,0.3)',
                        paddingTop: 5,
                    }}
                    current={new Date()}
                    monthFormat={'MMMM / yyyy'}
                    markingType={'custom'}
                    markedDates={{
                        '2019-03-23': {
                            selected: true, marked: true, customStyles: {
                                container: {
                                    backgroundColor: 'rgba(255,255,255,0.4)',
                                    elevation: 1,
                                    borderColor: '#fff',
                                    borderWidth: 1,
                                },
                                text: {
                                    color: 'white',
                                    fontSize: fontSize.xs,
                                    padding: 3,
                                    fontFamily: fontFamily
                                },
                            }
                        },
                    }}
                    theme={{
                        calendarBackground: 'transparent',
                        backgroundColor: 'rgba(255,255,255,0.3)',
                        dayTextColor: '#fff',
                        todayTextColor: 'blue',
                        // selectedDayTextColor: 'white',
                        monthTextColor: 'white',
                        textMonthFontSize: fontSize.sm,
                        textDayFontSize: fontSize.sm,
                        // selectedDayBackgroundColor: 'rgba(255,255,255,.6)',
                        arrowColor: 'white',
                        // textDisabledColor: 'red',
                        textDayFontFamily: fontFamily,
                        textMonthFontFamily: fontFamily,
                        textDayHeaderFontFamily: fontFamily,
                        'stylesheet.calendar.header': {
                            week: {
                                marginTop: 5,
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                borderBottomWidth: 1,
                                borderBottomColor: 'rgba(255,255,255,.3)',
                            },
                        },
                        "stylesheet.day.single": {
                            base: {
                                margin: 0,
                                padding: 0,
                            },
                            text: {
                                padding: 3,
                                // margin: 2,
                                fontSize: fontSize.sm,
                                color: '#fff',
                                fontFamily: fontFamily
                            }
                        }
                    }}
                    hideArrows={false}
                />
            </View>
            <View style={{ flexDirection: 'row', marginHorizontal: 20, justifyContent: 'space-between', }}>
                <View style={{ height: 48, borderRadius: 12, backgroundColor: 'rgba(255,255,255,0.3)', alignItems: 'center', paddingHorizontal: 13, flexDirection: 'row', flex: 1, maxWidth: LargeDimension ? 190 : 150 }}>
                    <Image source={asset.clock} />
                    <BaseText children='12:30 - 14:40' style={{ color: '#fff', marginLeft: 9, }} />
                </View>
                <View style={{ height: 48, borderRadius: 12, backgroundColor: 'rgba(255,255,255,0.3)', alignItems: 'center', paddingHorizontal: 13, flexDirection: 'row', flex: 1, maxWidth: LargeDimension ? 137 : 100 }}>
                    <BaseText children='AM' style={{ color: '#fff', marginRight: 9, flex: 1, textAlign: 'center' }} />
                    <Image source={asset.clock} />
                </View>
            </View>
        </ScrollView>
    }
}