import React from 'react';
import { Text, View, StyleSheet, ScrollView, Image, FlatList } from "react-native";
import { asset, LargeDimension, screenWidth } from '../../config';
import { BaseText } from '../../component/custom/BaseText';
import { BaseButton } from 'react-native-gesture-handler';
import i18 from '../../i18'
import BaseBlock from '../../component/custom/BaseBlock';

export default class SelectPatient extends React.PureComponent {

  state = {
    chosen: 'offline',
    chosenpatient: 113,
    patient: [
      {},
      {
        id: '113',
        name: 'Maria Dam',
        avatar: "",
        history: "Đái tháo đường, tiểu buốt, ra nhiều mồ hôi, biếng ăn"
      },
      {
        id: '456',
        name: 'Maria Dam',
        avatar: "",
        history: "Đái tháo đường, tiểu buốt, ra nhiều mồ hôi, biếng ăn"
      },
      {
        id: '789',
        name: 'Maria Dam',
        avatar: "",
        history: "Đái tháo đường, tiểu buốt, ra nhiều mồ hôi, biếng ăn"
      },
      {
        id: '101',
        name: 'Maria Dam',
        avatar: "",
        history: "Đái tháo đường, tiểu buốt, ra nhiều mồ hôi, biếng ăn"
      }
    ]
  }

  addPatient = () => {
    this.setState({
      patient: [...this.state.patient, ...this.state.patient]
    })
  }

  renderSelectType = (type) => {

    let isMark = this.state.chosen == type;

    return <BaseBlock style={{ height: 72, marginTop: 16, backgroundColor: 'rgba(255,255,255,0.3)', marginHorizontal: 20 }} button onPress={() => {
      if (isMark) return;
      setTimeout(() => {
        this.setState({
          chosen: type
        })
      }, 150);
    }}>
      <View style={{ flexDirection: 'row' }}>
        <Image source={asset[isMark ? 'radioChecked' : 'radioUncheck']} style={{ height: 20, width: 20, marginLeft: 18, marginRight: 10 }} />
        <View>
          <BaseText children={i18.t(type)} style={{ color: '#fff' }} bold />
          <View style={{ flexDirection: 'row', marginTop: 11, }}>
            <BaseText children={i18.t('cost')} style={{ color: '#fff' }} />
            <BaseText children={'900k'} style={{ color: '#fff', marginLeft: 8 }} bold />
          </View>
        </View>

      </View>

    </BaseBlock>
  }

  render() {
    return <ScrollView style={{ flex: 1, }}
      keyboardShouldPersistTaps={'handled'}
    >
      {this.renderSelectType('online')}
      {this.renderSelectType('offline')}
      <BaseText children={i18.t('choosePatient')} style={{ marginTop: 50, color: '#fff', fontWeight: '500', marginLeft: 20 }} size='md' />
      <BaseText children={`4 ${i18.t('patient')}`} style={{ marginTop: 4, color: '#fff', marginLeft: 20 }} />
      <FlatList
        data={this.state.patient}
        keyExtractor={(el, id) => id + ''}
        horizontal={true}
        style={{ marginLeft: 20, paddingBottom: 45 }}
        renderItem={({ item, index }) => {
          if (!item.id) {
            return <View style={{
              width: screenWidth * .7, maxWidth: 280, backgroundColor: 'rgba(255,255,255,0.3)', height: 154,
              borderStyle: 'dashed', borderColor: '#fff', borderWidth: 1,
              marginTop: 19, borderRadius: 12, padding: 16, marginRight: 12, justifyContent: 'center', alignItems: 'center'
            }}>
              <BaseButton onPress={this.addPatient}>
                <Image source={asset.plus} />
              </BaseButton>
            </View>
          }
          return <View style={{ width: screenWidth * .7, maxWidth: 280, backgroundColor: 'rgba(255,255,255,0.3)', height: 154, marginTop: 19, borderRadius: 12, padding: 16, marginRight: index + 1 != this.state.patient.length ? 12 : 20 }}>
            <View style={{ flexDirection: 'row' }}>
              <Image source={asset.defautAVT} style={{ height: 31, width: 31 }} />
              <View style={{ flex: 1, justifyContent: 'center', marginLeft: 8 }}>
                <BaseText children={i18.t('code') + ' ' + item.id} style={{ color: '#fff' }} size='xs' />
                <BaseText children={item.name} style={{ marginTop: 2 }} size='md' />
              </View>
              <BaseButton onPress={() => { this.setState({ chosenpatient: item.id }); }} rippleColor='transparent'>
                <Image source={asset[this.state.chosenpatient == item.id ? 'radioChecked' : 'radioUncheck']} style={{ height: 26, width: 26.2 }} />
              </BaseButton>
            </View>
            <BaseText children={i18.t('history')} style={{ marginTop: 26, color: '#fff' }} size='xs' />
            <BaseText children={item.history} style={{ marginTop: 6, color: '#232d42' }} />
          </View>
        }}
      />
    </ScrollView>
  }
}