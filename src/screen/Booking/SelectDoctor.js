import React from 'react';
import { View, FlatList, Image } from 'react-native';
import { BaseTextInput, BaseInput } from '../../component/custom/BaseInput';
import { asset } from '../../config';
import { BaseText } from '../../component/custom/BaseText';
import { BaseButton } from 'react-native-gesture-handler';
import i18 from '../../i18';

export default class SelectDoctor extends React.PureComponent {

    state = {
        chosenDoctor: '',
        doctors: [
            {
                id: 1,
                avt: '',
                name: 'Cecelia Wright',
                hospital: 'Bệnh viện Bạch mai',
                rate: 3,
                nearTime: [
                    '12:24 - 14:30',
                    '12:24 - 14:30',
                    '12:24 - 14:30',
                    '12:24 - 14:30',
                    '12:24 - 14:30'
                ]
            },
            {
                id: 2,
                avt: '',
                name: 'Cecelia Wright',
                hospital: 'Bệnh viện Bạch mai',
                rate: 3,
                nearTime: [
                    '12:24 - 14:30',
                    '12:24 - 14:30',
                    '12:24 - 14:30',
                    '12:24 - 14:30',
                    '12:24 - 14:30'
                ]
            },
            {
                id: 3,
                avt: '',
                name: 'Cecelia Wright',
                hospital: 'Bệnh viện Bạch mai',
                rate: 3,
                nearTime: [
                    '12:24 - 14:30',
                    '12:24 - 14:30',
                    '12:24 - 14:30',
                    '12:24 - 14:30',
                    '12:24 - 14:30'
                ]
            },
            {
                id: 4,
                avt: '',
                name: 'Cecelia Wright',
                hospital: 'Bệnh viện Bạch mai',
                rate: 3,
                nearTime: [
                    '12:24 - 14:30',
                    '12:24 - 14:30',
                    '12:24 - 14:30',
                    '12:24 - 14:30',
                    '12:24 - 14:30'
                ]
            },
            {
                id: 5,
                avt: '',
                name: 'Cecelia Wright',
                hospital: 'Bệnh viện Bạch mai',
                rate: 3,
                nearTime: [
                    '12:24 - 14:30',
                    '12:24 - 14:30',
                    '12:24 - 14:30',
                    '12:24 - 14:30',
                    '12:24 - 14:30'
                ]
            }
        ]
    }


    render() {
        return <View style={{ flex: 1, maxWidth: 335, width: '100%', alignSelf: 'center', }}>
            <BaseInput containerStyle={{
                borderColor: '#fff', borderWidth: 1,
                marginTop: 46, height: 48,
                marginBottom: 16,
            }}
                icon={asset.find}
                iconStyle={{ height: 20, width: 20, marginLeft: 18, marginRight: 10, marginLeft: 18 }}
                placeholder='Ai đó'
            />
            <FlatList
                keyExtractor={el => el.id.toString()}
                data={this.state.doctors}
                contentContainerStyle={{ paddingBottom: 45 }}
                renderItem={({ item }) => {
                    return <View style={{ width: '100%', backgroundColor: 'rgba(255,255,255,0.3)', borderRadius: 12, paddingTop: 16, paddingLeft: 16, paddingBottom: 21, marginTop: 8 }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                            <Image source={asset.defautAVT} style={{ height: 30, width: 30 }} />
                            <View style={{ marginLeft: 9, flex: 1 }}>
                                <BaseText children={item.name} style={{ color: '#fff' }} size='md' bold />
                                <BaseText children={item.hospital} style={{ color: '#fff', marginTop: 2 }} size='xs' />
                            </View>
                            <BaseButton onPress={() => {
                                let { doctors } = this.state;

                                doctors = doctors.map(el => {
                                    if (el.id == item.id) {
                                        return {
                                            ...el, selected: true
                                        }
                                    } else {
                                        delete el.selected
                                        return el
                                    }
                                })

                                this.setState({ doctors });
                            }} rippleColor='transparent' style={{ marginRight: 18 }}>
                                <Image source={asset[item.selected ? 'radioChecked' : 'radioUncheck']} style={{ height: 26, width: 26 }} />
                            </BaseButton>
                        </View>
                        <BaseText children={i18.t('rate')} style={{ color: 'white', marginTop: 14, }} size='xs' bold />

                        <View style={{ flexDirection: 'row', marginVertical: 13 }}>
                            {
                                (new Array(5).fill(1)).map((el, index) => {
                                    return <Image key={index} source={asset[index < item.rate ? 'star' : 'starOutline']} style={{ marginRight: 11 }} />
                                })
                            }
                        </View>
                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <BaseText children={i18.t('nearTime')} style={{ color: 'rgb(35,45,66)', marginRight: 20, }} />
                            <FlatList
                                data={item.nearTime}
                                keyExtractor={el => Math.random().toString()}
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                                renderItem={({ item }) => {
                                    return <BaseText children={item} style={{ color: '#fff', marginRight: 25 }} bold />
                                }}
                            />
                        </View>
                    </View>
                }}
            />
        </View>
    }
}