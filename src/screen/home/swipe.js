import React from 'react';
import { View, Text } from 'react-native';
import api from '../../api';

export default Swipe = (props) => {
    return <View style={{ backgroundColor: 'white', borderRadius: api.convertPTtoPX(16), minHeight: api.convertPTtoPX(200), marginTop: api.convertPTtoPX(25), marginHorizontal: api.convertPTtoPX(16), padding: api.convertPTtoPX(16) }}>
        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', }}>
            <Text style={{ fontSize: api.convertPTtoPX(18), fontWeight: '500', color: 'black' }}>
                PKĐK NPT
        </Text>
            <Text style={{ fontSize: api.convertPTtoPX(12), fontWeight: '500', color: 'rgb(130, 136, 148)' }}>
                22 Dec 2019
        </Text>
        </View>
        <Text style={{ fontSize: api.convertPTtoPX(12), fontWeight: '500', color: 'rgb(130, 136, 148)', fontWeight: '500' }}>
            Bác sĩ Phong
    </Text>
        <Text style={{ fontSize: api.convertPTtoPX(12), fontWeight: '500', color: 'rgb(130, 136, 148)', marginTop: api.convertPTtoPX(47) }}>
            Chẩn đoán sơ bộ
    </Text>
        <Text style={{ fontSize: api.convertPTtoPX(18), fontWeight: '500', color: 'black', marginTop: api.convertPTtoPX(8) }}>
            Rối loạn tiêu hoá - Loạn khuẩn
    </Text>
        <Text style={{ fontSize: api.convertPTtoPX(12), fontWeight: '500', color: 'black', fontWeight: '500', marginTop: api.convertPTtoPX(56) }}>
            Chỉ định của bác sĩ
    </Text>
        <View style={{ flexDirection: 'row' }}>
            <Text style={{ fontSize: api.convertPTtoPX(14), fontWeight: '500', color: 'rgb(130, 136 ,148)', fontWeight: '500', flex: 1 }}>Nexium 500mg</Text>
            <Text style={{ fontSize: api.convertPTtoPX(12), fontWeight: '500', color: 'rgb(130, 136 ,148)', fontWeight: '500', flex: 1 }}>2v/ngày - trước ăn 30p</Text>
        </View>
        <View style={{ flexDirection: 'row' }}>
            <Text style={{ fontSize: api.convertPTtoPX(14), color: 'rgb(130, 136 ,148)', fontWeight: '500', flex: 1 }}>Alphachoay</Text>
            <Text style={{ fontSize: api.convertPTtoPX(12), color: 'rgb(130, 136 ,148)', fontWeight: '500', flex: 1 }}>1v - Sau ăn</Text>
        </View>
        <Text style={{ fontSize: api.convertPTtoPX(12), fontWeight: '500', color: 'black', fontWeight: '500', marginTop: api.convertPTtoPX(17) }}>
            Lịch hẹn tiếp theo
    </Text>
        <Text style={{ fontSize: api.convertPTtoPX(14), color: 'rgb(130, 136 ,148)', marginTop: api.convertPTtoPX(8) }}>31/12/2019</Text>
    </View>
}