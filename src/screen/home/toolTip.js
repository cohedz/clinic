import React from 'react';
import { View, Text } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { BaseButton } from 'react-native-gesture-handler';


export default ToolTip = (props) => {
    return <View style={{
        backgroundColor: 'white',
        // height: api.convertPTtoPX(96),
        width: api.convertPTtoPX(107), position: 'absolute', top: api.convertPTtoPX(160),
        right: api.convertPTtoPX(16),
        borderRadius: api.convertPTtoPX(8),
        zIndex: 10, borderWidth: 1, borderColor: '#ddd'
    }}>
        {
            props.item.map((el, index) => {
                return <ToolTipItem
                    key={index}
                    {...el}
                    dismiss={props.dismiss}
                />
            })
        }
    </View>
}


const ToolTipItem = (props) => {
    return <BaseButton style={{ flexDirection: 'row', paddingLeft: api.convertPTtoPX(18), paddingVertical: api.convertPTtoPX(6) }}
        onPress={() => {
            props.action();
            props.dismiss()
        }}
    >
        <Icon
            name={props.icon}
            size={api.convertPTtoPX(16)}
        />
        <Text style={{ fontSize: api.convertPTtoPX(12), color: 'rgb(130, 136, 148)', marginLeft: api.convertPTtoPX(9) }}>{props.text}</Text>
    </BaseButton>
}