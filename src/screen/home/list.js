import React from 'react';
import { View, Text, FlatList } from 'react-native';
import api from '../../api';

export default List = (props) => {

    let data = [1, 2, 3, 4, 5]

    return <FlatList
        data={data}
        keyExtractor={index => index}
        style={{  }}
        renderItem={() => {
            return <View style={{ backgroundColor: 'white', borderRadius: api.convertPTtoPX(16), marginBottom: api.convertPTtoPX(25), marginHorizontal: api.convertPTtoPX(16), padding: api.convertPTtoPX(16) }}>
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', }}>
                    <Text style={{ fontSize: api.convertPTtoPX(12), fontWeight: '500', color: 'rgb(130, 136, 148)', fontWeight: '500' }}>
                        Bác sĩ Phong
        </Text>
                    <Text style={{ fontSize: api.convertPTtoPX(12), color: 'rgb(130, 136, 148)' }}>
                        22 Dec 2019
            </Text>
                </View>
                <Text style={{ fontSize: api.convertPTtoPX(18), fontWeight: '500', color: 'black', marginTop: api.convertPTtoPX(5) }}>
                    Rối loạn tiêu hoá - Loạn khuẩn
        </Text>
                <View style={{ flexDirection: 'row', marginTop: api.convertPTtoPX(5) }}>
                    <Text style={{ fontSize: api.convertPTtoPX(14), fontWeight: '500', color: 'rgb(130, 136 ,148)', fontWeight: '500', flex: 1 }}>Nexium 500mg</Text>
                    <Text style={{ fontSize: api.convertPTtoPX(12), fontWeight: '500', color: 'rgb(130, 136 ,148)', fontWeight: '500', flex: 1 }}>2v/ngày - trước ăn 30p</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={{ fontSize: api.convertPTtoPX(14), color: 'rgb(130, 136 ,148)', fontWeight: '500', flex: 1 }}>Alphachoay</Text>
                    <Text style={{ fontSize: api.convertPTtoPX(12), color: 'rgb(130, 136 ,148)', fontWeight: '500', flex: 1 }}>1v - Sau ăn</Text>
                </View>
            </View>
        }}
    />
}