import React from 'react';
import { View, Text, StyleSheet, ImageBackground, PermissionsAndroid, Image, FlatList } from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import api from '../../api';

import TouchID from 'react-native-touch-id'
import { BaseText } from '../../component/custom/BaseText';
import { BaseInput } from '../../component/custom/BaseInput';
import { asset } from '../../config';
import { Head } from '../../component/custom/Nav';
import i18 from '../../i18';
// var TouchID = require('react-native-touch-id');

class Home extends React.Component {

    data = [
        {
            title: 'Viêm mũi họng dị ứng',
            detail: 'Bác sĩ Phong',
            weekday: 'Sun',
            day: '22'
        },
        {
            title: 'Viêm mũi họng dị ứng',
            detail: 'Bác sĩ Phong',
            weekday: 'Mon',
            day: '22'
        },
        {
            title: 'Viêm mũi họng dị ứng',
            detail: 'Bác sĩ Phong',
            weekday: 'Tue',
            day: '22'
        },
        {
            title: 'Viêm mũi họng dị ứng',
            detail: 'Bác sĩ Phong',
            weekday: 'Web',
            day: '22'
        }
    ]

    render() {
        // <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 1}} colors={['#7fb5ff',  '#144891',]} style={{ flex: 1 }}></LinearGradient>
        return <View style={{ flex: 1 }}>
            <Head
                left={<Image source={asset.menu} style={{ height: 14, width: 16 }} />}
                onLeftPress={() => { this.props.navigation.push('booking') }}
                right={<Image source={asset.defautAVT} style={{ height: 40, width: 40 }} />}
                onRightPress={() => { this.props.navigation.push('login') }}
            />

            <View style={{ flex: 1, paddingHorizontal: 20 }}>
                <BaseText
                    size='lg'
                    style={{ color: '#232D42' }}
                    children={i18.t('wel')}
                />

                <View style={{ height: 50, borderWidth: 1, borderColor: '#E5ECED', borderRadius: 4, marginTop: 36 }} />

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 41, marginBottom: 17 }}>
                    <BaseText
                        style={{ color: '#232D42', fontWeight: '400' }}
                        children={i18.t('order')}
                    />
                    <BaseText
                        style={{ color: '#232D42' }}
                        children={i18.t('viewAll') + '(8)'}
                    />
                </View>
                <FlatList
                    data={this.data}
                    // keyExtractor={item => item.toString()}
                    showsVerticalScrollIndicator={false}
                    renderItem={({ item }, index) => {


                        return <View key={index} style={{ height: 70, borderWidth: 1, borderRadius: 4, backgroundColor: 'white', borderColor: '#E5ECED', marginBottom: 10, flexDirection: 'row' }}>
                            <View style={{ width: 58, backgroundColor: 'rgba(111,148,255,0.06)', borderTopLeftRadius: 4, borderBottomLeftRadius: 4, justifyContent: 'center', alignItems: 'center' }}>
                                <BaseText
                                    style={{ color: '#7FB5FF', fontSize: 26 }}
                                    children={item.day}
                                />
                                <BaseText
                                    style={{ color: '#7FB5FF' }}
                                    children={item.weekday}
                                />
                            </View>
                            <View style={{ flex: 1, justifyContent: 'center', marginLeft: 15, }}>
                                <BaseText
                                    style={{ color: '#232D42' }}
                                    children={item.title}
                                />
                                <BaseText
                                    size='xs'
                                    style={{ color: '#9CA3B1' }}
                                    children={item.detail}
                                />
                            </View>
                        </View>
                    }}
                    style={{ flex: 1 }}
                />

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 11, marginBottom: 17 }}>
                    <BaseText
                        style={{ color: '#232D42', fontWeight: '400' }}
                        children={i18.t('promotion')}
                    />
                    <BaseText
                        style={{ color: '#232D42' }}
                        children={i18.t('viewAll') + '(8)'}
                    />
                </View>
                <View style={{ height: 200 }}>
                    <FlatList
                        data={this.data}
                        // keyExtractor={item => item.toString()}
                        showsHorizontalScrollIndicator={false}
                        horizontal={true}
                        renderItem={({ item }, index) => {
                            return <View style={{ height: 194, width: 228, backgroundColor: 'white', borderRadius: 4, borderColor: '#E5ECED', borderWidth: 1, borderTopWidth: 0, marginRight: 10, marginBottom: 10 }}>
                                <ImageBackground source={{ uri: 'https://www.image.ie/images/no-image.png' }} style={{ height: 96, borderTopLeftRadius: 4, borderTopRightRadius: 4 }} >
                                    <View style={{ width: 50, height: 40, borderBottomLeftRadius: 4, borderBottomRightRadius: 4, backgroundColor: 'white', left: 162, justifyContent: 'center', alignItems: 'center' }}>
                                        <BaseText style={{ color: '#FF8F3C', fontSize: 15 }}>30%</BaseText>

                                    </View>
                                    <View style={{ flexDirection: 'row', bottom: 8, position: 'absolute', left: 20 }}>
                                        {Array(5).fill(5).map((el, index) => {
                                            return <Icon name='star' key={index} style={{ color: index < 3 ? 'yellow' : 'white', marginLeft: 2 }} />
                                        })}
                                    </View>
                                </ImageBackground>
                                <BaseText
                                    style={{ color: '#232D42', marginLeft: 20, marginTop: 15 }}
                                    children={'Khám răng hàm mặt'}
                                />
                                <BaseText
                                    size='xs'
                                    style={{ color: '#9CA3B1', marginLeft: 20, marginTop: 2 }}
                                    children={'Bác sĩ trưởng khoa hàm mặt Nguyễn Đình Thanh'}
                                />
                            </View>
                        }}
                    />
                </View>
            </View>
        </View>


    }
}

let styles = StyleSheet.create({
    avt: { height: api.convertPTtoPX(64), width: api.convertPTtoPX(64), borderRadius: api.convertPTtoPX(32), borderWidth: api.convertPTtoPX(1), borderColor: 'rgb(250, 115, 196)' }
})

export default Home