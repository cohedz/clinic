import action_type from "../action_type";

let initState = {
    show:'',
    msg:''
};

const UIreducer = ( state = initState, action ) => {
    switch (action.type) {
        case action_type.SHOW_MSG:
            return {
                ...state,
                show: action_type.SHOW_MSG,
                msg: action.msg
            }
            break;
        case action_type.HIDE_UI:
            return {
                ...state,
                show: '',
            }
            break;
        default:
            return state
            break;
    }
}
export default UIreducer;