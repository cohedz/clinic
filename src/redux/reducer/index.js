import { combineReducers } from 'redux';
import UIstate from './uiReducer'
import { createStore, applyMiddleware } from 'redux';
import logger from 'redux-logger'

let rootReducer = combineReducers({
    UIstate
});
let store = createStore(rootReducer, applyMiddleware(logger));
export default store;
