export default action_type = {
    SHOW_MSG: 'SHOW_MSG',
    HIDE_UI: 'HIDE_UI',
    SHOW_CONFIRM_BOX: 'SHOW_CONFIRM_BOX',
    SHOW_LOADING: 'SHOW_LOADING',
    SHOW_CALL_SCREEN: 'SHOW_CALL_SCREEN',
    SHOW_BOOKING_SUCCESS: 'SHOW_BOOKING_SUCCESS'
}