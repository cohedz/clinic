import React from 'react';
import { Text, TextInput, View, Image } from 'react-native'
import { fontSize, fontFamily, color } from '../../config';
import api from '../../api';
import { Input } from 'react-native-elements';
import BaseBlock from './BaseBlock';
import BaseText from './BaseText'
export const BaseTextInput = (props) => {
    return <TextInput
        placeholderTextColor='white'
        {...props}
        ref={props.references}
        style={{
            fontSize: fontSize[props.size] || fontSize.sm,
            flex: 1,
            fontFamily: fontFamily,
            minHeight: 20,
            height: '100%',
            color: '#fff',
            ...props.style,
        }}
        secureTextEntry={props.pass ? true : false}
        underlineColorAndroid={'transparent'}
    />
}

export const BaseInput = (props) => {
    return <BaseBlock style={props.containerStyle}>
        {
            props.textLeft ? <Text style={{
                color: 'white', fontSize: fontSize.md,
                fontFamily: fontFamily,
                marginLeft: 13, marginRight: 8,
            }}>{props.textLeft}</Text>
                : <Image style={{ height: 16, width: 16, marginLeft: 22, marginRight: 18, ...props.iconStyle }} source={props.icon} />
        }
        <BaseTextInput {...props} />
    </BaseBlock>
}


BaseInput.defaultProps = {
    references: () => { }
}


