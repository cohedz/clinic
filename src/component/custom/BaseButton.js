import React from 'react';
import { TouchableOpacity, TouchableNativeFeedback, Platform } from 'react-native';

import { fontSize, fontFamily } from '../../config';

let BT = Platform.OS == 'ios' ? TouchableOpacity : TouchableNativeFeedback

export const BaseButton = (props) => {
    return <BT {...this.props}>
        {props.children}
    </BT>
}