import React from 'react';
import { View, ActivityIndicator, Text, Platform, StyleSheet } from 'react-native';
import action_type from '../../redux/action_type';
export default Loading = (props) => {
    if (props.show != action_type.SHOW_LOADING) {
        return null;
    }
    return <View style={styles.container}>
        <ActivityIndicator size={Platform.OS == 'ios' ? 0 : 30} color='green' style={styles.indicator} />
        <Text style={styles.text}>Processing</Text>
    </View>
}
const styles = StyleSheet.create({
    container: { paddingHorizontal: 30, paddingVertical: 10, borderRadius: 5, backgroundColor: 'white' },
    indicator: { marginVertical: Platform.OS == 'ios' ? 15 : 0 },
    text: { fontSize: 14, color: 'gray', marginTop: 10 }
})