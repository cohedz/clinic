import React from 'react';
import { View, Text, Modal, StyleSheet } from 'react-native';
import MSG_BOX from './msgBox';
import { connect } from 'react-redux'
import action_type from '../../redux/action_type';
import CONFIRM_BOX from './confirmbox';
import Loading from './loading';
import BookingSuccess from './BookingSuccess';

let self = this;
class UI extends React.PureComponent {

  state = {
    show: '',
    msg: '',
    action: () => {

      alert(1)
    }
  }

  constructor(props) {
    super(props);
    self = this;
  }

  static hideUI = () => {
    self.setState({
      show: '',
    })
  }

  static showMSG = (msg) => {
    self.setState({
      show: action_type.SHOW_MSG,
      msg
    })
  }


  static showConfirm = (msg, action) => {
    self.setState({
      show: action_type.SHOW_CONFIRM_BOX,
      msg,
      action
    })
  }

  static showLoading = () => {
    self.setState({
      show: action_type.SHOW_LOADING,
    })
  }

  static showBookingSuccess = (funcGotoHome = () => { }) => {
    self.setState({
      show: action_type.SHOW_BOOKING_SUCCESS,
      action: funcGotoHome
    })
  }


  render() {
    return <Modal
      transparent={true}
      visible={!!this.state.show}
    >
      <View style={styles.container}>
        <MSG_BOX {...this.state} />
        <CONFIRM_BOX {...this.state} />
        <Loading {...this.state} />
        <BookingSuccess {...this.state} {...this.props} />
      </View>
    </Modal>
  }
}

let styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: 'rgba(51,129,235,.7)', alignItems: 'center', justifyContent: 'center' }
})
export default UI;