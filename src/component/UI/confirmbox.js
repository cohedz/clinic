import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import action_type from '../../redux/action_type';
import api from '../../api';
import i18 from '../../i18';
import Base_Button from '../custom/Base_Button';
import UI from '.';

const CONFIRM_BOX = (props) => {
    if (props.show !== action_type.SHOW_CONFIRM_BOX) {
        return null;
    }
    return <View style={styles.container}>
        <View style={styles.wrap_title}>
            <Text style={styles.title}>{i18.t('msg')}</Text>
        </View>
        <View style={styles.wrap_content}>
            <Text style={styles.content}>{props.msg}</Text>
        </View>
        <View style={{ flexDirection: 'row' }}>
            <Base_Button style={{ ...styles.wrap_button, borderBottomLeftRadius: 3, backgroundColor: '#aaa' }} onPress={UI.hideUI}>
                <Text style={styles.button_content} >
                    {i18.t('no').toUpperCase()}
                </Text>
            </Base_Button>
            <Base_Button style={{ ...styles.wrap_button, borderBottomRightRadius: 3, }} onPress={props.action}>
                <Text style={styles.button_content} >
                    {i18.t('yes').toUpperCase()}
                </Text>
            </Base_Button>
        </View>
    </View>
}

let styles = StyleSheet.create({
    container: { backgroundColor: 'white', width: api.screenWidth * .7, minHeight: 300, borderRadius: 3 },
    wrap_title: { borderBottomColor: '#ddd', borderBottomWidth: 1 },
    title: { fontSize: 20, fontWeight: '500', alignSelf: 'center', padding: 10 },
    wrap_content: { flex: 1, justifyContent: "center", alignItems: 'center' },
    content: { fontSize: 17 },
    wrap_button: { flex: 1, backgroundColor: 'blue', alignItems: "center" },
    button_content: { padding: 10, color: 'white', fontSize: 18 }
});
export default CONFIRM_BOX;