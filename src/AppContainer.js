// import React from 'react';
import { createStackNavigator, createAppContainer, createBottomTabNavigator, createDrawerNavigator } from 'react-navigation';

import SignUp from './screen/signUp';
import InitScreen from './screen';
import Home from './screen/home';
import Chat from './screen/chat';
import Notification from './screen/notification';
import Setting from './screen/setting';

import Icon from 'react-native-vector-icons/FontAwesome';
import HomeCall from './screen/HomeCall';
import CallScreen from './screen/CallScreen';
import ForgotPassword from './screen/ForgotPassword';
import Login from './screen/login';
import Booking from './screen/Booking';
import ViewTest from './screen/ViewTest';
import VerifyOTP from './screen/verifyOTP';
import DrawerPanel from './component/DrawerPanel';


let navigationOptions = (nameIcon) => ({
    tabBarIcon: (p) => {
        return <Icon
            name={nameIcon}
            color={p.tintColor}
            size={24}
        />
    }
})

const TabNavigator = createBottomTabNavigator({
    home: {
        screen: Home,
        navigationOptions: navigationOptions('home')
    },
    chat: {
        screen: Chat,
        navigationOptions: navigationOptions('comment')
    },
    notification: {
        screen: Notification,
        navigationOptions: navigationOptions('bell')
    },
    setting: {
        screen: Setting,
        navigationOptions: navigationOptions('cog')
    }
}, {
        tabBarOptions: {
            showLabel: false, activeTintColor: '#F8F8F8', // active icon color
            inactiveTintColor: '#586589',  // inactive icon color
            style: {
                backgroundColor: '#171F33' // TabBar background
            }
        }
    })


const DrawerNavigator = createDrawerNavigator({
    home: {
        screen: Booking,
    },
}, {
        contentComponent: DrawerPanel
    });


const AppNavigator = createStackNavigator(
    {
        login: Login,
        signUp: SignUp,
        forgotPassword: ForgotPassword,
        initScreen: InitScreen,
        home: DrawerNavigator,
        verifyOTP: VerifyOTP,
        homeCall: {
            screen: HomeCall
        },
        callScreen: {
            screen: CallScreen
        },
        booking: {
            screen: Booking
        },
        viewtest: ViewTest
    },
    {
        headerMode: 'none',
        initialRouteName: 'booking'
    }
);

const AppContainer = createAppContainer(AppNavigator);
export default AppContainer;