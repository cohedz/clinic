import React from 'react';
import { Provider } from 'react-redux';
import store from './redux/reducer'
import UI from './component/UI';
import SplashScreen from 'react-native-splash-screen'
import { messaging, notifications } from 'react-native-firebase';
import AppContainer from './AppContainer';
import { StatusBar } from 'react-native'

export default class App extends React.Component {

    // constructor(props) {
    //     super(props);

    //     messaging().hasPermission()
    //         .then(enabled => {
    //             if (enabled) {
    //                 console.log('====================================');
    //                 console.log('user has permissions');
    //                 console.log('====================================');
    //             } else {
    //                 console.log('====================================');
    //                 console.log('user doesnt have permission');
    //                 console.log('====================================');
    //                 try {
    //                     firebase.messaging().requestPermission();
    //                     console.log('====================================');
    //                     console.log('User has authorised');
    //                     console.log('====================================');
    //                 } catch (error) {
    //                     console.log('====================================');
    //                     console.log('User has rejected permissions');
    //                     console.log('====================================');
    //                 }
    //             }
    //         });

    //     messaging().getToken().then(token => {
    //         console.log('====================================');
    //         console.log(token);
    //         console.log('====================================');
    //     });

    // }

    componentDidMount = () => {
        SplashScreen.hide();
        // this.notificationDisplayedListener = notifications().onNotificationDisplayed((notification) => {
        //     console.log('====================================');
        //     console.log(notification);
        //     console.log('====================================');
        //     // Process your notification as required
        //     // ANDROID: Remote notifications do not contain the channel ID. You will have to specify this manually if you'd like to re-display the notification.
        // });
        // this.notificationListener = notifications().onNotification((notification) => {
        //     console.log('====================================');
        //     console.log(notification);
        //     console.log('====================================');
        //     // Process your notification as required
        // });
    }

    // componentWillUnmount() {
    //     this.notificationDisplayedListener();
    //     this.notificationListener();
    // }

    render() {
        return <Provider store={store}>
            <StatusBar barStyle='light-content' />
            <AppContainer />
            <UI />
        </Provider>
    }
}