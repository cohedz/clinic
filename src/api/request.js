import config from "../config";

export default request = (url, data, token, method = 'POST') => {
    url = config.domain + url;
    let option = {
        method,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': token
        },

    };
    if (method.toLowerCase() != 'get') {
        option.body = JSON.stringify(data)
    }

    return fetch(url, option).catch(err => err).then(async rs => {
        let data = {
            err: rs.status == 200 ? 0 : rs.status,
            ... await rs.json()
        }
        return data;
    })
}